package com.KoSeongho.Scan_Raspberry_Beacon;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class PopUpActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Eliminate title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
//        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
//        layoutParams.dimAmount = 0.7f;
//        getWindow().setAttributes(layoutParams);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.activity_pop_up);

        //update distance textView
        TextView textView = (TextView)findViewById(R.id.popup_distance_text);
        Intent intent = getIntent();
        String distance = intent.getStringExtra("distance");
        textView.setText(distance);


//        Intent intent = getIntent();
//        String data = intent.getStringExtra("data");

    }

    //close popup click 'OK' button
    public void onButtonClick(View v) {
        finish();
    }

    //close popup only 'OK' button
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
            return false;
        }else
            return true;
//        return super.onTouchEvent(event);
    }

    //defend back button
    @Override
    public void onBackPressed() {
        return;
    }
}
