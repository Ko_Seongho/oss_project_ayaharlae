package com.KoSeongho.Scan_Raspberry_Beacon;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity
{
    Vibrator vibrator;
    GpsInfo gps;

    private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;
    private Handler scanHandler = new Handler();
    private boolean isScanning = false;

    double longitude;
    double latitude;

    static int index = 0;
    static int Receive_cnt = 0;
    static int Current_index = 0;

    Context context = this;
//    Context context;

    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init BLE
        btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();

        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

        gps = new GpsInfo(MainActivity.this);

        if (gps.isGetLocation()) {

            gps.getLocation();
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            // GPS 를 사용할수 없으므로
            gps.showSettingsAlert();
        }
        scanHandler.post(scanRunnable);

//        Intent intent = new Intent(context, PopUpActivity.class);
//        intent.setFlags(Inte)
//        Intent intent = new Intent(context, PopUpActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
////        TextView Alarm_distance_view = (TextView)findViewById(R.id.popup_distance_text);
//        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Start Bluetooth LE Scan
    private Runnable scanRunnable = new Runnable()
    {
        @Override
        public void run() {
            if (!isScanning) {
                if(btAdapter != null) {
                    btAdapter.startLeScan(leScanCallback);
                    gps.getLocation();
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                }
            }
        }
    };

    //Bicycle
    public class bicycle {
        String name = null;
        double longitude;
        double latitude;
        String time;
    }

    //Bluetooth LE Scan
    public BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {

        bicycle[] bi_list = new bicycle[100];

        @Override
        public void onLeScan (final BluetoothDevice device, final int rssi, final byte[] scanRecord) {

            int startByte = 2;
//            int i;

            //Init PatternFound
            boolean patternFound = false;

            String Mac_address;

            TextView Result_Level = (TextView)findViewById(R.id.Alarm);
            TextView Reecive_cnt_view = (TextView)findViewById(R.id.Receive);
            TextView Distance_view = (TextView)findViewById(R.id.Distance);

            gps.getLocation();
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            //Check Raspberry Pi 3 Bluetooth LE Common Address - 'B8:27:EB...'
            //Do Not Use Identifier iBeacon
//            if (device.getAddress().toString().matches("B8:27:EB.*")) {
//                patternFound = true;
//                startByte = 5;
//            }
            //If Not Raspberry Pi 3 Bluetooth LE Address Do Not Find Pattern(UUID, Major, Minor)
            while (startByte <= 5)
            {
//                gps.getLocation();
//                latitude = gps.getLatitude();
//                longitude = gps.getLongitude();

                if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                        ((int) scanRecord[startByte + 3] & 0xff) == 0x15 &&
                        device.getAddress().toString().matches("B8:27:EB.*")
                        )
                { //Identifies correct data length
                    patternFound = true;
                    break;
                }
                startByte++;
            }

            //Correct Raspberry Pi 3 Bluetooth LE Common Address
            if (patternFound) {
                Mac_address = device.getAddress().toString();
                Log.d("MainActivity", Mac_address);

                double lat_temp;
                double lon_temp;

                String Result_isInAlramRange;

                //Interpret iBeacon Message Array to hex
                byte[] uuidBytes = new byte[16];
                System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                String hexString = bytesToHex(uuidBytes);

                //Set Vibration Pattern
//                long[] pattern_lv1 = {0, 500};
//                long[] pattern_lv2 = {0, 500, 200, 500, 200};
//                long[] pattern_lv3 = {0, 500, 200, 500, 200, 500, 200};
                long[] pattern_lv4 = {0, 500, 200, 500, 200, 500, 200, 500, 200};

                //Cut UUID Structure
                // 00000000000000000000000000000000
                //
                // 0000 -> Speed => 00.00 km/h
                // 0000 -> Heading => 000.0
                // 000000 -> Time => 00:00:00
                // 00000000 -> Latitude 00.000000
                // 000000000 -> Longitude 000.000000
                // 0 -> Direction[Binary] => NE NW SE SW
                String heading = hexString.substring(4,7) + "." + hexString.substring(7,8); // 000.0
                String lat = hexString.substring(14,16) + "." + hexString.substring(16,22); // 00.000000
                String lon = hexString.substring(22,25) + "." + hexString.substring(25,31); // 000.000000
                String time = hexString.substring(8,14); // 00:00:00
//                String speed = hexString.substring()

                //Receive count Raspberry Pi 3 Bluetooth LE Beacon
                //Max Size 1000
                if (Receive_cnt > 1000)
                    Receive_cnt = 0;

                Receive_cnt++;
                Reecive_cnt_view.setText(String.valueOf(Receive_cnt));

                double heading_double = Double.parseDouble(heading);
//                Log.d("Mainactivity", heading);
                double bi_lat =  Double.parseDouble(lat);
                double bi_lon =  Double.parseDouble(lon);

                double my_lat = latitude;
                double my_lon = longitude;

                double distance = Range.calDistance(my_lat, my_lon, bi_lat, bi_lon);
                String distance_str = Double.toString(distance);
                Distance_view.setText(distance_str.toString());
//                int distance_int = (int)distance;
//                String distance_int_str = Integer.toString(distance_int);

                //PopUp Screen
                Intent intent = new Intent(context, PopUpActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                TextView Alarm_distance_view = (TextView)((Activity).context).findViewById(R.id.popup_distance_text);

                Current_index = 0;

                //Find Correct Bicycle Use Raspberry Pi 3 Address
                while (Current_index < index) {

                    //Name duplicated check
                    if (bi_list[Current_index].name.equals(Mac_address)) {

                        //Time duplcated check
                        if (!bi_list[Current_index].time.equals(time)) {
                            lat_temp = bi_list[Current_index].latitude;
                            lon_temp = bi_list[Current_index].longitude;
                            bi_list[Current_index].latitude = latitude;
                            bi_list[Current_index].longitude = longitude;
                            bi_list[Current_index].time = time;

                            //find heading if GPS do not send heading
                            if (heading.equals("9999")) {
                                double Cur_Lat_radian = lat_temp * (3.141592 / 180);
                                double Cur_Lon_radian = lon_temp * (3.141592 / 180);
                                double Dest_Lat_radian = bi_list[Current_index].latitude * (3.141592 / 180);
                                double Dest_Lon_radian = bi_list[Current_index].longitude * (3.141592 / 180);
                                double radian_distance = 0;

                                radian_distance = Math.acos(Math.sin(Cur_Lat_radian)
                                        * Math.sin(Dest_Lat_radian) + Math.cos(Cur_Lat_radian)
                                        * Math.cos(Dest_Lat_radian)
                                        * Math.cos(Cur_Lon_radian - Dest_Lon_radian));

                                double radian_bearing = Math.acos((Math.sin(Dest_Lat_radian)
                                        - Math.sin(Cur_Lat_radian)
                                        * Math.cos(radian_distance))
                                        / (Math.cos(Cur_Lat_radian) * Math.sin(radian_distance)));

                                double true_bearing;
                                if (Math.sin(Dest_Lon_radian - Cur_Lon_radian) < 0) {
                                    true_bearing = radian_bearing * (180 / 3.141592);
                                    true_bearing = 360 - true_bearing;
                                } else {
                                    true_bearing = radian_bearing * (180 / 3.141592);
                                }
                                heading_double = true_bearing;

                                //Check Risk Level
                                Result_isInAlramRange = Range.isInAlramRange(bi_lon, bi_lat, heading_double, my_lon, my_lat);
                                double distance_temp = Range.calDistance(my_lat, my_lon, bi_lat, bi_lon);
                                int distance_int = (int)distance_temp;
                                String distance_int_str = Integer.toString(distance_int);

                                //Alarm Risk Level, Vibration, PopUp_Screen
                                if (Result_isInAlramRange == null) {
                                    Result_Level.setText("안죽어 걱정마");
                                } else {
                                    //Check Risk Level
                                    Result_Level.setText(Result_isInAlramRange.toString());
//                                    if (Result_isInAlramRange.toString().equals("====== 레벨 1 ======")) {
//                                    } else if (Result_isInAlramRange.toString().equals("====== 레벨 2 ======")) {
//                                    } else if (Result_isInAlramRange.toString().equals("====== 레벨 3 ======")) {
//                                    } else
                                    if (Result_isInAlramRange.toString().equals("====== 레벨 4 ======")) {
                                        vibrator.vibrate(pattern_lv4, -1);
//                                        Alarm_distance_view.setText(distance_int_str.toString());
                                        intent.putExtra("distance",distance_int_str.toString());
                                        startActivity(intent);
                                        //Only Action Vibration Risk_Level_4
                                    }
                                }
                            } else { //GPS send heading
                                if (heading_double <= 360.0) {

                                    //Check Risk Level
                                    Result_isInAlramRange = Range.isInAlramRange(bi_lon, bi_lat, heading_double, my_lon, my_lat);
                                    double distance_temp = Range.calDistance(my_lat, my_lon, bi_lat, bi_lon);
                                    int distance_int = (int)distance_temp;
                                    String distance_int_str = Integer.toString(distance_int);

                                    //Alarm Risk Level, Vibration, PopUp_Screen
                                    if (Result_isInAlramRange == null) {
                                        Result_Level.setText("안죽어 걱정마");
                                    } else {
                                        Result_Level.setText(Result_isInAlramRange.toString());
//                                        if (Result_isInAlramRange.toString().equals("====== 레벨 1 ======")) {
//                                        } else if (Result_isInAlramRange.toString().equals("====== 레벨 2 ======")) {
//                                        } else if (Result_isInAlramRange.toString().equals("====== 레벨 3 ======")) {
//                                        } else
                                        if (Result_isInAlramRange.toString().equals("====== 레벨 4 ======")) {
                                            vibrator.vibrate(pattern_lv4, -1);
//                                            Alarm_distance_view.setText(distance_int_str.toString());
                                            intent.putExtra("distance",distance_int_str.toString());
                                            startActivity(intent);
                                            //Only Action Vibration Risk_Level_4
                                        }
                                    }
                                }
                            }
                            break;
                        } else { //If Time is same do Not Everything
                            break;
                        }
                    } else { //Search next Index Bicycle
                        Current_index++;
                    }
                }

                //New bicycle Add Bicycle_List [MAX_SIZE is 100] Do Not ues Dynamic Memory
                if (Current_index >= index) {
                    bi_list[Current_index] = new bicycle();
                    bi_list[Current_index].name = Mac_address;
                    bi_list[Current_index].latitude = latitude;
                    bi_list[Current_index].longitude = longitude;
                    bi_list[Current_index].time = time;
                    index++;

                    if (heading_double <= 360.0) {
                        //Check Risk Level
                        Result_isInAlramRange = Range.isInAlramRange(bi_lon, bi_lat, heading_double, my_lon, my_lat);
                        double distance_temp = Range.calDistance(my_lat, my_lon, bi_lat, bi_lon);
                        int distance_int = (int)distance_temp;
                        String distance_int_str = Integer.toString(distance_int);

                        //Alarm Risk Level, Vibration, PopUp_Screen
                        if (Result_isInAlramRange == null) {
                            Result_Level.setText("안죽어 걱정마");
                        } else {
                            Result_Level.setText(Result_isInAlramRange.toString());
//                                        if (Result_isInAlramRange.toString().equals("====== 레벨 1 ======")) {
//                                        } else if (Result_isInAlramRange.toString().equals("====== 레벨 2 ======")) {
//                                        } else if (Result_isInAlramRange.toString().equals("====== 레벨 3 ======")) {
//                                        } else
                            if (Result_isInAlramRange.toString().equals("====== 레벨 4 ======")) {
                                vibrator.vibrate(pattern_lv4, -1);
//                                Alarm_distance_view.setText(distance_int_str.toString());
                                intent.putExtra("distance",distance_int_str.toString());
                                startActivity(intent);
                                //Only Action Vibration Risk_Level_4
                            }
                        }
                    }
                }
            }

        }
    };

    //bytesToHex method
    static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    private static String bytesToHex(byte[] bytes)
    {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ )
        {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


    //Range method
    public static class Range {

        public static double bp_x;         //자전거와 보행자 경도 차
        public static double bp_y;         //자전거와 보행자 위도 차

        private static double distance;      //자전거 보행자 거리(m)

        private static double theta;      //보행자-자전거와 n극-자전거 사잇각

        //Calculate Person - Bicycle Distance
        public static double calDistance(double lat1, double lon1, double lat2, double lon2) {
            double theta, dist;

            theta = lon1 - lon2;
            dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1))
                    * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1609.344; // Distance measure - meter(m)

            return dist;
        }

        // 주어진 도(degree) 값을 라디언으로 변환
        private static double deg2rad(double deg) {
            return (double) (deg * Math.PI / (double) 180d);
        }

        // 주어진 라디언(radian) 값을 도(degree) 값으로 변환
        private static double rad2deg(double rad) {
            return (double) (rad * (double) 180d / Math.PI);
        }

        //Alarm Risk Level
        public static String setAlramlv(double distance){
            String Result_setAlramlv = null;
            if(distance <= 18){
                Result_setAlramlv = "====== 레벨 4 ======";
            }else if(distance <= 24){
                Result_setAlramlv = "====== 레벨 3 ======";
            }else if(24 < distance && distance <= 40){
                Result_setAlramlv = "====== 레벨 2 ======";
            }else if(40 < distance && distance <= 50) {
                Result_setAlramlv = "====== 레벨 1 ======";
            }else {
                Result_setAlramlv = "안죽어 걱정마";
            }
            return Result_setAlramlv;
        }

        //Check Risk Level Range
        public static String isInAlramRange(double b_x, double b_y, double heading, double p_x, double p_y) {

            String Result_isInAlramRange = null;

            bp_x = deg2rad(p_x) - deg2rad(b_x);
            bp_y = deg2rad(p_y) - deg2rad(b_y);

            theta = Math.atan(bp_x / bp_y);
            theta = rad2deg(theta);

            if (bp_y >= 0 && bp_x < 0) {
//                if (bp_x >= 0)
//                    theta = theta;
//                else
//                    theta = theta + 360;
                theta = theta + 360;
            } else {
                theta = theta + 180;
            }

            distance = calDistance(b_x, b_y, p_x, p_y);

            if(heading >= 0 && heading < 60){
                if((heading+300 <= theta)  && (theta <= 360)){
                    Result_isInAlramRange = setAlramlv(distance);
                }
            }else if(heading > 300 && heading <= 360){
                if(heading - 60 <= theta && theta <= heading){
                    Result_isInAlramRange = setAlramlv(distance);
                }
            }else {
                if (heading - 60 <= theta && theta <= heading + 60) {
                    Result_isInAlramRange = setAlramlv(distance);
                }
            }
            return Result_isInAlramRange;
        }
    }
}