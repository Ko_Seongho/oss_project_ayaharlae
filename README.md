# 2017 오픈소스 개발 경진대회 - 아야할래 #

자전거와 보행자간 안전 응용을 위한 BLE기법을 이용한 네트워크 기법

### 개발환경 및 개발언어 ###

* RaspBerryPi-3 : Rasbian with Python
- The Raspberry Pi 3 source code was uploaded into the pdf.

* Android : Android Studio with Java

### OpenSource ###

* Bluez
* gattlib
* bluetooth
* pybluez
* iBeacon

![스크린샷 2017-08-16 오후 4.23.21.png](https://bitbucket.org/Ko_Seongho/oss_project_ayaharlae/src/72a4f0a03b3e77b2d13f9ccaac4f0e75b5b89655/images/스크린샷%202017-08-16%20오후%204.23.21.png?at=master)